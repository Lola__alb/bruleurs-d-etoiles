<?php
$today = date('Y-m-d');
$upcoming_events = new WP_Query(array(
    'post_type' => 'post', 
    'meta_key' => 'date-fin',
    'orderby' => 'meta_value',
    'order' => 'ASC',
    'posts_per_page' => 2,
    'paged' => get_query_var('paged'),
    'meta_query' => array(
        array(
            'key' => 'date-fin',
            'value' => $today,
            'compare' => '>=',
            'type' => 'DATE',
        )
    )
));


$past_events = new WP_Query(array(
    'post_type' => 'post', 
    'meta_key' => 'date-fin',
    'orderby' => 'meta_value',
    'order' => 'DESC',
    'posts_per_page' => 3,
    'paged' => get_query_var('paged'),
    'meta_query' => array(
        array(
            'key' => 'date-fin',
            'value' => $today,
            'compare' => '<',
            'type' => 'DATE',
        )
    )
));

//Gérer dynamiquement la répartition du nombre d'articles par page entre à venir et passés
// $upcoming_events_count = $upcoming_events->found_posts;
// $past_events_count = $past_events->found_posts;

// $upcoming_posts_per_page = ($upcoming_events_count > 5) ? 5 : $upcoming_events_count;
// $past_posts_per_page = ($upcoming_posts_per_page == 5) ? 0 : (5 - $upcoming_posts_per_page);

// $upcoming_events = new WP_Query(array(
//     'post_type' => 'post', 
//     'meta_key' => 'date-fin',
//     'orderby' => 'meta_value',
//     'order' => 'ASC',
//     'posts_per_page' => $upcoming_posts_per_page,
//     'paged' => get_query_var('paged'),
//     'meta_query' => array(
//         array(
//             'key' => 'date-fin',
//             'value' => $today,
//             'compare' => '>=',
//             'type' => 'DATE',
//         )
//     )
// ));

// $past_events = new WP_Query(array(
//     'post_type' => 'post', 
//     'meta_key' => 'date-fin',
//     'orderby' => 'meta_value',
//     'order' => 'DESC',
//     'posts_per_page' => $past_posts_per_page,
//     'paged' => get_query_var('paged'),
//     'meta_query' => array(
//         array(
//             'key' => 'date-fin',
//             'value' => $today,
//             'compare' => '<',
//             'type' => 'DATE',
//         )
//     )
// ));

$date_str = get_post_meta(get_the_ID(), 'date-debut', true); 
if (!empty($date_str)) {
    $date = DateTime::createFromFormat('Y-m-d', $date_str);
    $date_fr = $date->format('d F Y');
    echo $date_fr;
}



?>

<?php 
if($upcoming_events->have_posts()) {
    echo '<h1>Evénements à venir</h1>';
    echo '<div class="event-list to-come">';
    while($upcoming_events->have_posts()) {
        $upcoming_events->the_post(); 
        echo '<div class="event-item">'; 
        echo '<a href="'.get_the_permalink().'"><span class="link-spanner">'.get_the_title().'</span></a>';
        echo '<div class="event-left">';
        echo '<div class="event-thumbnail">'.the_post_thumbnail('mini').'</div>';
        echo '<div class="event-data"><h3>'.get_the_title().'</h3><p>'.get_the_excerpt().'</p></div>';
        echo '</div>';
        echo '<p>'.date_i18n('d M Y', strtotime(get_post_meta(get_the_ID(), 'date-debut', true))).'</p>';
        echo '</div>';
    }
    echo '</div>';
}
?>



<h2>Evénements passés</h2>
<div class="event-list is-over">
    <?php while($past_events->have_posts()): $past_events->the_post(); ?>
        <div class="event-item">
            <a href="<?php the_permalink();?>"><span class="link-spanner"><?php the_title();?></span></a>
            <div class="event-left">
                <div class="event-thumbnail">
                    <?php the_post_thumbnail('mini');?>
                </div> 
                <div class="event-data">
                    <h3>
                        <?php the_title();?>
                    </h3>
                    <p><?php the_excerpt('excerpt_more');?></p>
                </div>
            </div>      
            <p><?php echo date_i18n('d M Y', strtotime(get_post_meta(get_the_ID(), 'date-debut', true))); ?></p>
        </div>
    <?php endwhile; ?>
</div>

<div class="pagination-event">
<?php 

// Pagination
$big = 99; // valeur arbitraire
echo paginate_links( array(
    'base'    => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
    'format'  => '?paged=%#%',
    'current' => max( 1, get_query_var('paged') ),
    'total'   => $upcoming_events->max_num_pages + $past_events->max_num_pages
) );

?> 
</div>


<?php wp_reset_postdata(); ?>
