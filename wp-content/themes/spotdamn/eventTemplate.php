<?php /* Template Name: eventTemplate */ ?>
<?php get_header(); ?>
<?php 

;?>
<main id="event-template-main">
    <section class="page-content">
        <?php get_template_part('loop-content-page');?>
    </section>

    <?php wp_reset_postdata();?>

    <section class="event-bloc">
        <?php get_template_part('event-list-page');?>
    </section>
</main>
<?php get_footer(); ?>

