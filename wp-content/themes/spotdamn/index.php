<?php get_header(); ?>
<section class="hero">
    <div class="hero-inner">
        <p><?php _e("lorem ipsum lorem ipsum lorem lorem lorem");?></p>
    </div>
</section>
<main>
    <section class="article-section">
        <?php 
            $args = array('post_type' => 'post', 'order' => 'ASC');
            $the_query = new WP_Query($args);
        ;?>
        <?php if($the_query->have_posts()):?>
            <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>
            <article class="article-bloc" id="post-<?php the_id();?>">
                    <div class="post-thumbnail">
                        <?php the_post_thumbnail('thumbnail');?>
                    </div>
                    <section class="article-content">
                        <h2 class="article-title">
                            <a href="<?php the_permalink();?>">
                                <?php the_title();?>
                            </a>
                        </h2>
                        <div class="article-info">
                                <?php _e('Publié le ');?><?php the_date();?> <?php _e(' par ');?><?php the_author();?>
                        </div>
                        <div class="article-content">
                            <?php the_excerpt('excerpt_more');?>
                        </div>
                    </section>        
            </article>
            <?php endwhile;?>
            <?php else:?>
                <?php _e("La page ne contient pas d'articles");?>
        <?php endif;?>
    </section>
</main>
<?php get_footer(); ?>
