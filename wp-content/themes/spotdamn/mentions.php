<?php /* Template Name: mentions*/ ?>
<?php get_header(); ?>
<main id="event-template-main">
    <section class="page-content">
        <?php get_template_part('loop-content-page');?>
    </section>
</main>
<?php get_footer(); ?>
