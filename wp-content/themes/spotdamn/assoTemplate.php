<?php /* Template Name: assoTemplate */ ?>
<?php get_header(); ?>
<main id="asso-template-main">
    <section class="page-content">
        <?php get_template_part('loop-content-page');?>
    </section>
    <section>
        <?php dynamic_sidebar( 'asso-chiffres' ); ?>
    </section>
</main>
<?php get_footer(); ?>

