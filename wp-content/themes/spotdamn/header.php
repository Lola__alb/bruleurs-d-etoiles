<!DOCTYPE html>
<html <?= language_attributes(); ?>>
    <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
   <title><?php bloginfo('name'); ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>" type="text/css" />
    <?php wp_head(); ?>
    </head>
<body <?= body_class()?>>
    <header>
        <div class="header-illu"></div>
        <div class="header-bloc">
            <div class="header-logo"> 
                    <?php echo the_custom_logo();?>
                    <p><?php echo get_bloginfo('description');?></p>
            </div>
            <nav>
                <?= wp_nav_menu([
                    "theme_location" => "main-menu"
                ]);?>
                <div class="search-icon">
                    <i class="fa-solid fa-magnifying-glass"></i>
                </div>
                <a href="#" class="toggle-button" id="menu">
                    <span class="bar"></span>
                    <span class="bar"></span>
                    <span class="bar"></span>
                </a>
                <aside class="sidebar">
                    <?php dynamic_sidebar( 'searchbar' ); ?>
                </aside>
            </nav>
        </div>
    </header>
