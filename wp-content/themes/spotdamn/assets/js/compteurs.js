/*Appel des fonctions après chargement*/
window.addEventListener('DOMContentLoaded', () => {
    counters()
})

/*Compteurs association*/
function counters(){
    compteur1 = document.querySelector('.widget-event')
    compteur2 = document.querySelector('.widget-concert')
    compteur3 = document.querySelector('.widget-participants')
    timer = 0
    counter1 = 0
    counter2 = 0
    counter3 = 0
    limit1 = 6
    limit2 = 50
    limit3 = 200
    counts = setInterval(()=>{
        timer++
        if(counter3 < limit3){
            counter3++
            compteur3.textContent = counter3
        }
        if(timer % 4 == 0){
            if(counter2 < limit2){
                counter2++
                compteur2.textContent = counter2
            }
        }
        if(timer % 34 == 0){
            if(counter1 < limit1){
                counter1++
                compteur1.textContent = counter1
            }
        }
        if(timer == 500){
            clearInterval(counts)
        }       
    }, 25)
}