/*Appel des fonctions après chargement*/
window.addEventListener('load', () => {
    document.getElementById('menu').addEventListener('click', displayMenu);
    document.getElementById('menu').addEventListener('click', menuTransition);
    navbarColors();
    iconSearch();
    document.querySelector('.search-icon i').addEventListener('click', displaySearchBar);
})

/*Menu burger pour le mobile*/
function displayMenu(){
    items = document.querySelectorAll(".menu-item") 
    for(i=items.length - 1;i >= 0; i--){
        items[i].classList.toggle("visible") 
    }
    burger = document.getElementById('menu')
    burger.classList.toggle('menu-open'); 
    
    navbarColors()
}

/*Transition à l'ouverture du menu burger en mobile (apparaît par la droite) */
function menuTransition(){
    burger = document.getElementById('menu')
    bars = document.querySelectorAll('.bar')
    list = document.querySelector('nav ul')
    if(burger.classList.contains('menu-open')){
        list.style.height = "100vh";
        for(i=0;i<bars.length;i++){
            bars[i].style.backgroundImage="linear-gradient(-45deg, #C1E8BD, #019B94, #C1E8BD)"
            bars[i].style.backgroundColor = "black"
        }
    }
    else{
        list.style.height = "0";
        for(i=0;i<bars.length;i++){
            bars[i].style.backgroundImage="none"
            bars[i].style.backgroundColor = "#0d0f12"
        }
    }
}

/*Barre de recherche*/
function displaySearchBar(){
    searchIconIcon = document.querySelector('.search-icon i');

    searchBar = document.querySelector('.sidebar');
    searchBar.classList.toggle('sidebar-visible');

    if(searchBar.classList.contains('sidebar-visible')){
        searchIconIcon.classList.remove('fa-magnifying-glass')
        searchIconIcon.classList.add('fa-xmark')
    }else{
        searchIconIcon.classList.remove('fa-xmark')
        searchIconIcon.classList.add('fa-magnifying-glass')
    }
    navbarColors()
}

//couleurs de la navbar et de la searchbar
function navbarColors(){
    navbar = document.querySelector('nav')
    bar = document.querySelector('.sidebar')
    theicon = document.querySelector('.search-icon i')
    burgermenu = document.getElementById('menu')

    if(window.innerWidth > 768){
        console.log('desktop')
        if(bar.classList.contains('sidebar-visible')){
            theicon.style.color = "white"
            console.log('white')
        }else{
            theicon.style.color="black"
        }

    }else if(window.innerWidth <768){
        if(burgermenu.classList.contains('menu-open')){
            theicon.style.color='white';
            navbar.style.backgroundImage = 'none';

        }else{
            navbar.style.backgroundImage = 'linear-gradient(-45deg, #C1E8BD, #019B94)';
            theicon.style.color='black'
        }
    }
}

/*Disparition de la banière au scroll*/
window.onscroll = function() {
    let prevScrollpos = window.pageYOffset;
    
    let header = document.querySelector(".header-bloc");
    let logo = document.querySelector('.header-logo');
    let headerBottom = header.offsetTop + header.offsetHeight;
    let currentScrollPos = window.pageYOffset;
    
        if (prevScrollpos <= currentScrollPos && currentScrollPos > 60){
             logo.style.height = "0px";
             logo.style.margin = "0px";
             logo.style.marginBottom = "0px";
             logo.style.paddingBottom = "0px";
             header.style.height = "70px"
        }
        else{  if (currentScrollPos < 60) {
            logo.style.height = "60px";
            logo.style.margin = "10px";
            logo.style.marginBottom = "15px";
            logo.style.paddingBottom = "15px";  
            header.style.height = "150px"
        }
    }
    prevScrollpos = currentScrollPos;
    }


    /*Searchbar icone */
function iconSearch(){
    iconS=document.querySelector('.wp-block-search__button')
    return iconS.innerHTML = "<i class='fa-solid fa-magnifying-glass'></i>"
}