    <footer>
        <?php wp_footer();?>
        <img class="footer-image" src="<?php echo get_template_directory_uri() . '/assets/images/feu_coin-05.svg';?>" alt=" ">
        <section class="footer-bloc">
            <div class="footer-logo">
                <img class="footer-image-logo" src="<?php echo get_template_directory_uri() . '/assets/images/logo-petit.svg';?>" alt="Logo Brûleurs d'étoiles">
            </div>
            <div class="footer-menu"> 
                <?= wp_nav_menu([
                                "theme_location" => "footer-menu"
                ]);?>
            </div>
            <div class="footer-copyright">
                <?php _e("Copyright 2023");?>
            </div>
        </section>
    </footer>
</body>
</html>