<?php get_header();?>
<main>

    <?php if(have_posts()):?>
        <?php while (have_posts()) : the_post(); ?>
        <article id="post-<?php the_id();?>">
            <header>
                <h1 class="single-article-title"><?php the_title();?></h1>
                <div class="single-article-date">                   
                    <p><?php echo date_i18n('d M Y', strtotime(get_post_meta(get_the_ID(), 'date-debut', true))) . " - " . date_i18n('d M Y', strtotime(get_post_meta(get_the_ID(), 'date-fin', true))); ?></p>
                 
                </div>
                <div class="single-article-content">
                    <?php the_content();?>
                </div>
            </header>
        </article>
        <?php endwhile;?>
    <?php endif;?>
</main>
<?php get_footer();?>