<?php if(have_posts()):?>
    <?php while (have_posts()) : the_post(); ?>
    <article id="post-<?php the_id();?>">
            <div class="page-content">
                <?php the_content();?>
            </div>
    </article>
    <?php endwhile;?>
    <?php else:?>
        <?php _e("Il n'y a rien à voir ici");?>
<?php endif;?>