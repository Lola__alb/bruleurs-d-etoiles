<?php /* Template Name: contactTemplate */ ?>
<?php get_header(); ?>
<main id="asso-template-main">
    <section class="page-content">
        <?php get_template_part('loop-content-page');?>
    </section>
    <section class="contact-form-content">
        <?php echo do_shortcode( '[contact-form-7 id="57" title="Formulaire de contact"]' ); ?>
    </section>
</main>
<?php get_footer(); ?>
