
<?= 
//Miniatures des articles sur la page d'accueil
add_theme_support('post-thumbnails');

add_image_size( 'post-image', 350, 250);
add_filter( 'image_size_thumbnail', 'custom-thumbnail' );
function image_size_thumbnail( $sizes ) {
    return array_merge( $sizes, array(
        'post-image' => __( 'Post Images' ),
    ) );
}

//Miniature des articles sur la page événements
add_image_size( 'mini', 100, 200);
add_filter( 'image_size_mini', 'custom-thumbnail' );
function image_size_mini( $sizes ) {
    return array_merge( $sizes, array(
        'mini' => __( 'Mini Images' ),
    ) );
}

//Menus
function register_my_menus() { 
    register_nav_menus(
        array(
            'main-menu' => 'menu-principal',
            'footer-menu' => 'menu-secondaire'
        )
    ); 
}
add_action( 'init', 'register_my_menus' );

//Logo 
function themename_custom_logo_setup() {
$defaults = array(
    // 'height'              => 100,
    // 'width'                => 400,
    'flex-height'          => true,
    'flex-width'           => true,
    'header-text'          => array( 'site-title', 'site-description' ),

    'unlink-homepage-logo' => true, 

);

add_theme_support( 'custom-logo', $defaults );

};
add_theme_support('custom-logo');

//Trouver et charger les scripts js
function get_script_url() {
    $url = get_stylesheet_directory_uri() . '/assets/js/custom.js';
    return $url;
}

function add_js_scripts(){
    wp_enqueue_script('custom-js', get_script_url());
};

add_action('wp_enqueue_scripts', 'add_js_scripts');


//Script du template association
function get_script_asso_url() {
    $url = get_stylesheet_directory_uri() . '/assets/js/compteurs.js';
    return $url;
}

function add_script_asso() {
    if (is_page_template('assoTemplate.php')) {
      wp_enqueue_script('compteurs-js', get_script_asso_url());
    }
  }
  add_action('wp_enqueue_scripts', 'add_script_asso');

//Script des mentions légales
function get_script_mentions_url() {
    $url = get_stylesheet_directory_uri() . '/assets/js/mentions.js';
    return $url;
}

function add_script_mentions() {
    if (is_page_template('mentions.php')) {
      wp_enqueue_script('mentions-js', get_script_mentions_url());
    }
  }
  add_action('wp_enqueue_scripts', 'add_script_mentions');


//Script du fond 
function get_script_bg_url() {
    $url = get_stylesheet_directory_uri() . '/assets/js/background.js';
    return $url;
}

function add_script_bg() {
    if (is_page_template('eventTemplate.php') || is_page_template('contactTemplate.php') || is_page_template('assoTemplate.php') || is_single()) {
      wp_enqueue_script('background-js', get_script_bg_url());
    }
  }
  add_action('wp_enqueue_scripts', 'add_script_bg');


//Texte du "voir plus" après un paragraphe tronqué
// function excerpt_text($more) {
//     global $post;
//     return ' ... <a href="'. get_permalink($post->ID) . '"> Voir plus</a>';
// }
// add_filter('excerpt_more', 'excerpt_text');

// function excerpt_text($more) {
//     if ( is_home() ) {
//         global $post;
//         return ' ... <a href="'. get_permalink($post->ID) . '"> Voir plus</a>';
//     } else{
//         global $post;
//         return ' ... <a href="'. get_permalink($post->ID) . '"></a>';
//     }
// }
// add_filter('excerpt_more', 'excerpt_text');

//Taille des extraits sur la page des événements
function short_excerpt_length( $length ) {
    if ( is_page( 'evenements' ) ) {
        return 30; 
    } else {
        return $length;
    }
}
add_filter( 'excerpt_length', 'short_excerpt_length');

//Nombre d'articles sur la page des événements
function custom_events_query( $query ) {
    if ( $query->is_main_query() && is_page( 'evenements' ) ) {
        $query->set( 'posts_per_page', 5 );
    }
}
add_action( 'pre_get_posts', 'custom_events_query' );

//widgets compteurs
function wpb_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Chiffres association', 'text-domain' ),
        'id'            => 'asso-chiffres',
        'description'   => __( 'Génère les chiffres concernant l\'association', 'text-domain' ),
        'before_widget' => '<div class="asso-chiffres-bloc">',
        'after_widget'  => '</div>',
        'before_title'  => '<h2 class="widget-title">',
        'after_title'   => '</h2>',
    ) );
}
add_action( 'widgets_init', 'wpb_widgets_init' );

//widget searchbar
add_theme_support( 'title-tag' );
add_theme_support( 'html5', array( 'search-form' ) );

function searchbar_widget(){
    register_sidebar( array(
        'id' => 'search-sidebar',
        'name' => 'searchbar',
    ));
}
add_action( 'widgets_init', 'searchbar_widget' );


//Gérer les catégories des articles et leur affichage
function no_category( $query ) {
    if ( $query->is_home() && $query->is_main_query() ) {
    $query->set( 'cat', '1');
    }
   }
   add_action( 'pre_get_posts', 'no_category' );
?>