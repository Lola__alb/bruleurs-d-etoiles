<?php get_header();?>
<main>
    <?php if(have_posts()):?>
        <?php while(have_posts()) : the_post();?>
            <h2><?php the_title();?></h2>
            <div class="page-content">
                <?php the_content();?>
            </div>
        <?php endwhile;?>
    <?php endif;?>
</main>
<?php get_footer();?>